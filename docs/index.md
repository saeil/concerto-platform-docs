![Logo](assets/images/logo.png)

# Welcome to the concerto platform documentation

This documentation is for [concertoplatform.com](https://concertoplatform.com).
The open-source online adaptive testing platform.

## Quick reference
* Website: [https://concertoplatform.com]()
* Wiki: [https://github.com/campsych/concerto-platform/wiki]()
* Issue tracker: [https://github.com/campsych/concerto-platform/issues]()
* Docker Hub: [https://hub.docker.com/r/campsych/concerto-platform]()
* Source code: [https://github.com/campsych/concerto-platform]()
* Changelog: [https://github.com/campsych/concerto-platform/blob/master/CHANGELOG.md]()
* Upgrade: [https://github.com/campsych/concerto-platform/blob/master/UPGRADE.md]()


## Support for older versions
Documentation for releases **up to v5.0.beta.7** can be found [here](https://github.com/campsych/concerto-platform/wiki/Support-for-older-versions).

